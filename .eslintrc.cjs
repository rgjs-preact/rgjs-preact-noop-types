
module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
  },
  extends: [
    'preact',
  ],
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 12,
    sourceType: 'module',
  },
  ignorePatterns: [
    'build/',
  ],
  rules: {
    'prettier/prettier': 0,
    'no-unused-vars': ['warn', { 'args': 'none' }],
    'no-multiple-empty-lines': ['error', {max: 1, maxBOF: 1}],
    'indent': ['error', 2, {'SwitchCase': 1, 'MemberExpression': 0, }],
    'linebreak-style': ['error', 'unix'],
    'quotes': ['error', 'single'],
    'semi': ['error', 'always'],

    'react/prefer-stateless-function': [0],
    'react/no-find-dom-node': [0],

    'no-useless-constructor': 0,
  },
};
