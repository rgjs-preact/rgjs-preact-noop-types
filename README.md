# rgjs-preact-noop-types

`prop-types`, but only in dev.

## Getting started

```bash
npm install rgjs-preact-noop-types
```

## Development

### Installation

Installation

```bash
git clone https://gitlab.com/rgjs-preact/rgjs-preact-noop-types.git
cd rgjs-preact-transition-group && git config --local core.hooksPath .githooks/ && npm i
```

## Contribute

[File a bug](https://gitlab.com/rgjs-preact/rgjs-preact-noop-types/-/issues) and I'll try to help.

## License

MIT &copy; REJH Gadellaa
