
let PropTypes;

if ( process.env.NODE_ENV === 'production' ) {

  const fns = [
    'oneOfType',
    'oneOf',
  ];

  const noop = () => null;
  const fn = noop;
  fn.isRequired = noop;

  PropTypes = new Proxy( {}, {
    get ( target, prop ) {
      if ( fns.includes( prop ) )
        return () => fn;
      return fn;
    },
  });

}
else {
  PropTypes = require('prop-types');
}

export default PropTypes;
